<?php
 
if(isset($_POST['email'])) {
 
     
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $email_to = "clickitplugins@gmail.com";
 
    $email_subject = "Support From Clickit Plugins Website";
 
     
 
     
 
    function died($error) {
 
        // your error code can go here
 
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";
 
        die();
 
    }
 
     
 
    // validation expected data exists
 
    if(!isset($_POST['name']) ||
 
        !isset($_POST['email']) ||
 
        !isset($_POST['url']) ||
 
        !isset($_POST['plugin']) ||

        !isset($_POST['version']) ||
        
        !isset($_POST['message']) 
        ) {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
    $plugin = $_POST['plugin']; // not required

    $version = $_POST['version']; // not required
 
    $name = $_POST['name']; // required
 
    $email = $_POST['email']; // required
 
    $url = $_POST['url']; // required
 
    $message = $_POST['message']; // not required
 
 
 
     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }

 

  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
    $email_message .= "Plugin: ".clean_string($plugin)."\n"; 

    $email_message .= "Version: ".clean_string($version)."\n";    

    $email_message .= "Name: ".clean_string($name)."\n";
 
    $email_message .= "Email: ".clean_string($email)."\n";
 
    $email_message .= "Website: ".clean_string($url)."\n";
 
    $email_message .= "Message: ".clean_string($message)."\n";
 
     
 
     
 
// create email headers
 
$headers = 'From: '.$email."\r\n".
 
'Reply-To: '.$email."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
header("Location: https://www.clickitplugins.com/success");
 
?>
 
 
 
<!-- include your own success html here -->
 
 
 

 
 
<?php
 
}
 
?>