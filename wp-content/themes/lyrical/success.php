<?php /* Template Name: Success */ ?>

<!DOCTYPE html>
<head>
 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Clickit Plugins | Premium WordPress Plugins</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simple, Responsive & Highly Customizable plugins to add an extra functionality into your site." />
	
	<link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png">


	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/simple-line-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200' rel='stylesheet' type='text/css'>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

	
	<!-- Modernizr JS -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
	#fh5co-header #navbar li a span:before {
    content: "";
    position: absolute;
    width: 100%;
    height: 2px;
    bottom: 0;
    left: 0;
    background-color: rgb(3, 147, 137);
    visibility: hidden;
    -webkit-transform: scaleX(0);
    -moz-transform: scaleX(0);
    -ms-transform: scaleX(0);
    -o-transform: scaleX(0);
    transform: scaleX(0);
    -webkit-transition: all 0.3s ease-in-out 0s;
    -moz-transition: all 0.3s ease-in-out 0s;
    -ms-transition: all 0.3s ease-in-out 0s;
    -o-transition: all 0.3s ease-in-out 0s;
    transition: all 0.3s ease-in-out 0s;
}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92795754-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body style="background: #f3f3f3;">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7423KS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header role="banner" id="fh5co-header">
		<div class="fluid-container">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
					<a class="navbar-brand" href="https://www.clickitplugins.com"><span>Clickit Plugins</span></span></a> 
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a id="home_link" href="https://www.clickitplugins.com"><span>Home</span></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>

		<section id="fh5co-home" data-section="home" style="height:417px;">
			<div class="gradient"></div>
			<div class="container">
				<div class="text-wrap" style="    height: 417px;">
					<div class="text-inner">
						<div class="row" style="width:100%;margin-top: 113px;">
							<div class="col-md-8 col-md-offset-2" style="width: 100%;">
								<h1 class="to-animate">Thank You!</h1>
								<h3 class="to-animate" style="margin: 0 auto; width: 70%;"><h3 class="to-animate">Your message has been sent successfully, Clickit Plugin's support team will be in touch with you very soon.</h3></h3>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


<!-- jQuery -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
<!-- Owl Carousel -->
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<!-- Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/google_map.js"></script>
<!-- Main JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!-- Hammer JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/hammer.js"></script>
<!-- ImageLoaded JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.js"></script>
<!-- Sequence JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sequence.js"></script>

<script type="text/javascript">
	var sequenceElement = document.getElementById("sequence");
	var sequenceElement2 = document.getElementById("sequence2");


	var options = {
		keyNavigation: true,
		animateCanvas: true,
		phaseThreshold: false,
		reverseWhenNavigatingBackwards: true,
		autoPlay : true,
		autoPlayInterval: 2000
	}

	var mySequence = sequence(sequenceElement, options);
	var mySequence2 = sequence(sequenceElement2, options);

</script>
<script type="text/javascript">
		$('#home_link').click(function(){
   window.location.href='https://www.clickitplugins.com/';
})
	</script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
</body>
</html>
