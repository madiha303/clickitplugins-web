<?php /* Template Name: Facebook Feed */ ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Facebok Feed Plugin let you create your Facebook Page Feed in a stylish way to show your Facebook Page statuses on your WordPress site" />
    <meta name="keywords" content="wp Facebook feed, Facebook feed, Facebook feed, wordpress Facebook feed, wp fb feed, Facebook plugin, wp Facebook plugin, wp fb plugin" />
    <meta name="author" content="ArrowPlugins" />
    <meta name="msvalidate.01" content="5C9D99C96DDE5C5A9895D4A1E481D1B4" />
    <meta name="google-site-verification" content="piICf3uM39zgLVKK9Ynr69U-R2WEYQIxAmqoqaHkUik" />
    <title>Facebook Feed Plugin | Premium WordPress Plugin</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/landing-page.css" rel="stylesheet">


<meta property="og:title" content="Facebook Feed Plugin | Premium WordPress Plugin">
<meta property="og:description" content="Facebok Feed Plugin let you create your Facebook Page Feed in a stylish way to show your Facebook Page statuses on your WordPress site">
<meta property="og:image" content="https://www.arrowplugins.com/wp-content/uploads/2017/01/Banner.png">
<meta property="og:url" content="https://www.arrowplugins.com/wordpress-facebook-feed">
<meta property="og:type" content="website">


<meta name="twitter:title" content="Facebook Feed Plugin | Premium WordPress Plugin ">
<meta name="twitter:description" content="Facebok Feed Plugin let you create your Facebook Page Feed in a stylish way to show your Facebook Page statuses on your WordPress site">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:image" content="https://www.arrowplugins.com/wp-content/uploads/2017/01/Banner.png" />



    <!-- Custom Fonts -->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png">
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/icomoon.css">
    <!-- Simple Line Icons -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/simple-line-icons.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

     <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/animate.css/animate.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/flexslider/flexslider.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
    <link id="color-scheme" href="<?php echo get_template_directory_uri(); ?>/assets/css/colors/default.css" rel="stylesheet">
    <!-- Modernizr JS -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">

<style type="text/css">

    .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
        margin: 0 auto;
    }
    #uc_link:hover{
    color: #d3edff;
    text-decoration: underline;
    }
    #uc_share:hover{
        color: white !important;
        text-decoration: underline;
    }
    
    body{
        font-family: 'Josefin Sans', sans-serif !important;
    }
    #fh5co-header #navbar li a span:before {
    content: "";
    position: absolute;
    width: 100%;
    height: 2px;
    bottom: 0;
    left: 0;
    background-color: rgb(3, 147, 137);
    visibility: hidden;
    -webkit-transform: scaleX(0);
    -moz-transform: scaleX(0);
    -ms-transform: scaleX(0);
    -o-transform: scaleX(0);
    transform: scaleX(0);
    -webkit-transition: all 0.3s ease-in-out 0s;
    -moz-transition: all 0.3s ease-in-out 0s;
    -ms-transition: all 0.3s ease-in-out 0s;
    -o-transition: all 0.3s ease-in-out 0s;
    transition: all 0.3s ease-in-out 0s;
}
</style>
<script
   id="fsc-api"
   src="https://d1f8f9xcsvx3ha.cloudfront.net/sbl/0.7.2/fastspring-builder.min.js"
   type="text/javascript"
   data-storefront="arrowplugins.onfastspring.com/popup-store">
</script>
<!-- Begin Inspectlet Embed Code -->

<style type="text/css">
    img#wpstats{
        display: none !important;
    }#more-info-button:hover{
    color: white;
}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92795754-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7423KS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--     <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav> -->

   <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="http://www.clickitplugins.com">Clickit Plugins</a>
          </div>
          <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
                  <li><a href="http://www.clickitplugins.com">Home</a></li>
                  <li><a href="http://www.clickitplugins.com/support">Support</a></li>
              </li>
              <!--li.dropdown.navbar-cart-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown')-->
              <!--        span.icon-basket-->
              <!--        |-->
              <!--        span.cart-item-number 2-->
              <!--    ul.dropdown-menu.cart-list(role='menu')-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-9.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Short striped sweater-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .navbar-cart-item.clearfix-->
              <!--                .navbar-cart-img-->
              <!--                    a(href='#')-->
              <!--                        img(src='assets/images/shop/product-10.jpg', alt='')-->
              <!--                .navbar-cart-title-->
              <!--                    a(href='#') Colored jewel rings-->
              <!--                    |-->
              <!--                    span.cart-amount 2 &times; $119.00-->
              <!--                    br-->
              <!--                    |-->
              <!--                    strong.cart-amount $238.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                .cart-sub-totle-->
              <!--                    strong Total: $476.00-->
              <!--        li-->
              <!--            .clearfix-->
              <!--                a.btn.btn-block.btn-round.btn-font-w(type='submit') Checkout-->
              <!--li.dropdown-->
              <!--    a.dropdown-toggle(href='#', data-toggle='dropdown') Search-->
              <!--    ul.dropdown-menu(role='menu')-->
              <!--        li-->
              <!--            .dropdown-search-->
              <!--                form(role='form')-->
              <!--                    input.form-control(type='text', placeholder='Search...')-->
              <!--                    |-->
              <!--                    button.search-btn(type='submit')-->
              <!--                        i.fa.fa-search-->
            </ul>
          </div>
        </div>
      </nav>
    <div class="intro-header" style="background: #f3f3f3;padding-top: 0;padding-bottom: 0;background: url('<?php echo get_template_directory_uri(); ?>/images/feed-bg.png');">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message" style="padding-top: 11%;
    padding-bottom: 4%;">
    <h3 class="to-animate fadeInUp animated" style="font-family: 'Josefin Sans', sans-serif !important;margin: 0;text-shadow: none;font-size: 4em;color:#e0e0e0;margin-top:40px;font-weight: 100;">WORDPRESS FACEBOOK FEED</h3>

    <h2 class="to-animate fadeInUp animated" style="color:#e0e0e0;font-family: 'Josefin Sans', sans-serif !important;    line-height: 1.5;
    font-size: 35px;    font-size: 25px;
    width: 60%;
    MARGIN: 0 AUTO;
    margin-top: 30px;

    margin-bottom: 30px;
    font-size: 2em;
    ">Build <strong>Grids, Thumbnail View, Blog Style Feed or Vertical Feed</strong> to display your Portfolio or Facebook Page Posts On your WordPress site!</h2>
        <h2 class="to-animate fadeInUp animated" style="color:#e0e0e0;font-family: 'Josefin Sans', sans-serif !important;    line-height: 1.5;
    font-size: 35px;    font-size: 25px;
    width: 60%;
    MARGIN: 0 AUTO;
    margin-top: 30px;

    margin-bottom: 30px;
    font-size: 2em;
    "><strong>Ready to use, Responsive, Customizable...</strong> </h2>
                      
                       <div class="call-to-action">
                        <a href='#premium-packages' 
                       
                        id="more-info-button" 
                        class=" fadeInUp animated" 
                        style="font-size: 23px;" 
                      >
                        Get it Now! $15</a>
                          <a href='https://wordpress.org/plugins/wp-facebook-feed/' 
                        id="more-info-button" 
                        class="download to-animate fadeInUp animated" 
                        style="font-size: 23px;" 
                        >
                        Get Free Version</a>
                                               </div>

                          
                </div>

            </div>

        </div>

        <!-- /.container -->
    </div>
    <!-- /.intro-header -->
 <div  class="to-animate fadeInUp animated" style="background: white;padding-top: 0;padding-bottom: 20px;text-align: center;font-family: 'Josefin Sans', sans-serif !important;border: 2px solid #e9e9e9;
    padding-top: 20px;">
    <h1 style="font-family: 'Josefin Sans', sans-serif !important;padding: 10px;margin: 30px;color: black;"><strong>READY-TO-USE</strong> FEED SKINS</h1>
   
   
     <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com -->
    <!-- This code works with jquery library. -->

<table style="margin: 0 auto;">
    <tr>
         <td style="display: inline-block;">
            <img style="width: 300px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/default-template.png">
        </td>
        <td style="display: inline-block;">
            <img style="width: 300px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/template-0.png">
        </td>
         <td style="display: inline-block;">
            <img style="width: 300px;
    display: block;
    margin: 10px;    " src="<?php echo get_template_directory_uri(); ?>/images/template-3.png">
        </td>
    </tr>
   <tr>
       <td style="display: inline-block;">
            <img style="width: 300px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/template-4.png">
        </td>/
        <td style="display: inline-block;">
            <img  style="width: 300px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/template-5.png">
        </td>
         <td style="display: inline-block;">
            <img  style="width: 300px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/template-6.png">
        </td>
    </tr>
</table>

</div>






 <div  class="to-animate fadeInUp animated" style="background: #323943;padding-top: 0;padding-bottom: 20px;text-align: center;font-family: 'Josefin Sans', sans-serif !important;border: 2px solid #e9e9e9;
    padding-top: 20px;">
    <h1 style="font-family: 'Josefin Sans', sans-serif !important;padding: 10px;margin: 30px;color: white;"><strong>FEED STYLES</strong></h1>
   
   
     <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com -->
    <!-- This code works with jquery library. -->

<table style="margin: 0 auto;text-align: center;font-weight: bold;color: white;">
    <tr>
        <td style="display: inline-block;">
        <p>Vertical</p>
            <img style="width: 240px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/vertical-feed.png">
        </td>
        <td style="display: inline-block;">
        <p>Thumbnails</p>

            <img style="width: 240px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/thumbnail-feed.png">
        </td>
         <td style="display: inline-block;">
        <p>Blog Style</p>

            <img style="width: 240px;
    display: block;
    margin: 10px;   border: 1px solid #d6d6d6; " src="<?php echo get_template_directory_uri(); ?>/images/blog-style-feed.png">
        </td>
       <td style="display: inline-block;">
        <p>Masonry</p>

            <img style="width: 240px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/masonry-feed.png">
        </td>
 <td style="display: inline-block;">
        <p>Masonry (Only Images)</p>

            <img  style="width: 240px;
    display: block;
    margin: 10px;    border: 1px solid #d6d6d6;" src="<?php echo get_template_directory_uri(); ?>/images/masonry-feed2.png">
        </td>
    </tr>
</table>

</div>
    <!-- Page Content -->

    <div class="content-section-a">
<div class="container" >
  <h1 style="    font-size: 3em;
    margin-bottom: 45px;
    color: black;text-shadow: none;font-family: 'Josefin Sans', sans-serif !important;text-align: center;">What You're Getting</h1>
    <div class="row">
     
        <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-diamond"></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">PREMIUM FEED SKINS & STYLES</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">6 Feed Skins & 4+ combinations of FEED Styles, Facebook Feed will look amazing on your posts, pages and in widget areas</p>
        </div>
         <div class="col-md-4 text-center" >
         <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-cogs "></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">QUICK & EASY SETUP</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">The Facebook Feed Plugin comes with a powerful admin panel which allows you to customize each of your Feed Style and Control the settings.</p>
         </div>
            <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-tablet "></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">RESPONSIVE DESIGN</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Facebook Feed Plugin comes build-in with responsive design to fit into your smaller devices like mobile & tablets.</p>
        </div>
    </div>
     <div class="row" style="margin-top: 50px;">
     
        <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-instagram"></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">USERNAME & HASHTAG SUPPORT</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">With Facebook Feed Plugin, you can fetch any feed from public facebook page and display onto your site</p>
        </div>
         <div class="col-md-4 text-center" >
         <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-bolt"></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">FAST & LIGHT WEIGHT</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Facebook Feed Plugin is very light weight Powered by HTML5 & CSS3 to ensure unparalleled performance so your website load fast with Feed plugin enabled</p>
         </div>
            <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-life-ring "></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">24/7 SUPPORT</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Need Help! With our Quality Product, you'll get Quality Support even with free version. Contact us anytime, we'll do our best to answer and resolve all your questions & issues as soon as possible</p>
        </div>

</div>
  </div>
  </div>
   

   <div class="content-section-a" style="padding-top: 17px;">
<div class="container" >
 
    <div class="row">
     
        <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-file-code-o"></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">UNLIMITED FEEDS</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Create Unlimited number of Facebook Feeds with unique customizations, Skins & Feed Styles from any of your facebook page of your choice</p>
        </div>
         <div class="col-md-4 text-center" >
         <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-code"></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">SHORTCODE SUPPORTED</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Add Feed into your posts, pages, home page or into widget area using shortcode and your facebook feed will show up instantly.</p>
         </div>
            <div class="col-md-4 text-center" >
        <i style="    font-size: 70px;
    margin-bottom: 20px;    color: #049389;" class="fa fa-refresh "></i>
            <h4 style="margin: 0;font-weight: bold;font-family: 'Josefin Sans', sans-serif !important;">LIFETIME UPDATES</h4>
            <p style="color: #5a5a5a;    margin-top: 13px;">Receive frequently updates & improvements. With Premium, You will get every bit of new feature, skins, styles etc in up coming updates as long as you use the plugin.</p>
        </div>
    </div>
    <style>
        .footable td{

  padding: 10px 20px;
  text-align: center;
  border: 1px solid #ccc;
        }
    </style>
     <h1 style="    font-size: 2em;
    margin-bottom: 34px;
    color: black;text-shadow: none;font-family: 'Josefin Sans', sans-serif !important;text-align: center;font-weight: bold;">Some More Cool Features</h1>
    <table class="footable" style="width: 100%;border: 1px solid black;font-weight: bold;font-size: 17px;
    margin-top: 50px;color: black;">
  <tbody>
    <tr id="test">
      <td>Vertical Style Feed</td>
      <td>Thumbnails Style Feed</td>
      <td>Masonry Style Feed</td>
    </tr>
    <tr>
      <td>Blog Style Feed</td>
      <td>Masonry Style Feed (Only Images)</td>
      <td>Get any public user feed</td>
    </tr>
    <tr>
      <td>Show or Hide Photo Post Date</td>
      <td>Create Unlimited number of Feeds</td>
      <td>Show Feed Using Shortcode</td>
    </tr>
    <tr>
      <td>Ability to get Number of Photos</td>
      <td>Create Multiple Column Masonry Feed</td>
      <td>Limit Post Caption Text</td>
    </tr>
    <tr>
     <td>Show or Hide Profile Picture</td>
      <td>Show or Hide Caption Text of Post</td>
      <td colspan="3">You can also Show Pictures only </td>
      
    </tr>
  </tbody>
</table>
  </div>
  </div>  
  <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        /* Carousel */

#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 30px;
    /* Control buttons  */
    /* Previous button  */
    /* Next button  */
    /* Changes the position of the indicators */
    /* Changes the color of the indicators */
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-control.left {
    left: -60px;
}
#quote-carousel .carousel-control.right {
    right: -60px;
}
#quote-carousel .carousel-indicators {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    margin: 5px;
    cursor: pointer;
    border: 4px solid #CCC;
    border-radius: 50px;
    opacity: 0.4;
    overflow: hidden;
    transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
    background: #333333;
    width: 128px;
    height: 128px;
    border-radius: 100px;
    border-color: #f33;
    opacity: 1;
    overflow: hidden;
}
.carousel-inner {
    min-height: 300px;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
    </style>



<script type="text/javascript">

</script>
	<a  name="contact"></a>
    <div class="banner" style="    background: #f3f3f3;
    color: black;">

       <div class="container" style="    margin-top: -50px;">

            <div class="row">
                <div class="col-lg-12 text-center" style="margin-bottom: 50px">









<style type="text/css">
    @import url(https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css);
@import url(https://fonts.googleapis.com/css?family=Raleway:400,500,800);
@import url(https://fonts.googleapis.com/css?family=Montserrat:800);
.snip1214 {
  font-family: 'Raleway', Arial, sans-serif;
  color: #000000;
  text-align: center;
  font-size: 19px;
  width: 100%;
  margin: 40px 10px;
  font-weight: bold;
}
.snip1214 .plan {
  margin: 0;
  width: 25%;
  position: relative;
  float: left;
  background-color: #ffffff;
  border: 1px solid rgba(0, 0, 0, 0.1);
}
.snip1214 * {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
.snip1214 header {
  position: relative;
}
.snip1214 .plan-title {
  position: relative;
  top: 0;
  font-weight: 800;
  padding: 5px 15px;
  margin: 0 auto;
  -webkit-transform: translateY(-50%);
  transform: translateY(-50%);
  margin: 0;
  display: inline-block;
  background-color: #222f3d;
  color: #ffffff;
  text-transform: uppercase;
}
.snip1214 .plan-cost {
  padding: 0px 10px 20px;
}
.snip1214 .plan-price {
  font-family: 'Montserrat', Arial, sans-serif;
  font-weight: 800;
  font-size: 2.4em;
  color: #34495e;
}
.snip1214 .plan-type {
  opacity: 0.6;
}
.snip1214 .plan-features {
  padding: 0;
  margin: 0;
  text-align: center;
  list-style: outside none none;
  font-size: 0.8em;
}
.snip1214 .plan-features li {
  border-top: 1px solid #d2d7e2;
padding: 18px 0% 18px 2%;
}
.snip1214 .plan-features li:nth-child(even) {
  background: rgba(0, 0, 0, 0.08);
}
.snip1214 .plan-features i {
  margin-left: 8px;
  opacity: 0.4;
}
.snip1214 .plan-select {
  border-top: 1px solid #d2d7e2;
  padding: 10px 10px 0;
}
.snip1214 .plan-select a {
  background-color: #039389;
  color: #ffffff;
  text-decoration: none;
  padding: 0.5em 1em;
  -webkit-transform: translateY(50%);
  transform: translateY(50%);
  font-weight: 800;
  text-transform: uppercase;
  display: inline-block;
  border-radius: 5px;
}
.snip1214 .plan-select a:hover {
  background-color: #04403c;
}
.snip1214 .featured {
  margin-top: -10px;
  background-color: #066b64;
  color: #ffffff;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.4);
  z-index: 1;
}
.snip1214 .featured .plan-title,
.snip1214 .featured .plan-price {
  color: #ffffff;
}
.snip1214 .featured .plan-cost {
  padding: 10px 10px 20px;
}
.snip1214 .featured .plan-features li {
  border-top: 1px solid rgba(255, 255, 255, 0.4);
}
.snip1214 .featured .plan-select {
  padding: 20px 10px 0;
  border-top: 1px solid rgba(255, 255, 255, 0.4);
}
@media only screen and (max-width: 767px) {
  .snip1214 .plan {
    width: 50%;
  }
  .snip1214 .plan-title,
  .snip1214 .plan-select a {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
  .snip1214 .plan-cost,
  .snip1214 .featured .plan-cost {
    padding: 20px 10px 20px;
  }
  .snip1214 .plan-select,
  .snip1214 .featured .plan-select {
    padding: 10px 10px 10px;
  }
  .snip1214 .featured {
    margin-top: 0;
  }
}
@media only screen and (max-width: 440px) {
  .snip1214 .plan {
    width: 100%;
  }
}

</style>
<h1 id="premium-packages" style="font-family: 'Josefin Sans', sans-serif !important;padding-bottom: 40px ;margin: 0 auto;text-align: center;"><strong>CHOOSE THE PERFECT PLAN </strong></h1>
<div  class="snip1214">
  <div class="plan">
    <h3 class="plan-title">
      Starter
    </h3>
    <div class="plan-cost"><span class="plan-price">$15</span><span class="plan-type">/ one-time</span></div>
    <ul class="plan-features" style="text-align: left;">
      <li><i class="ion-checkmark"> </i>1 WordPress site</li>
      <li><i class="ion-checkmark"> </i>3 months of support</li>
      <li><i class="ion-checkmark"> </i>3 months of premium updates</li>
      <li><i class="ion-checkmark"> </i>24/7 Tech Support</li>
    </ul>
    <div class="plan-select"><a href="" data-fsc-action="Add,Checkout" data-fsc-item-path-value="facebook-feed-permium" onclick="this.parentNode.submit(); return false;">BUY NOW</a></div>
  </div>
  <div class="plan">
    <h3 class="plan-title">
      Basic
    </h3>
    <div class="plan-cost"><span class="plan-price">$34</span><span class="plan-type">/ one-time</span></div>
    <ul class="plan-features" style="text-align: left;">
      <li><i class="ion-checkmark"> </i>3 WordPress sites</li>
      <li><i class="ion-checkmark"> </i>3 months of support</li>
      <li><i class="ion-checkmark"> </i>3 months of premium updates</li>
      <li><i class="ion-checkmark"> </i>24/7 Tech Support</li>
    </ul>
    <div class="plan-select"><a href="" data-fsc-action="Add,Checkout" data-fsc-item-path-value="facebook-feed-basic" onclick="this.parentNode.submit(); return false;">BUY NOW</a></div>
  </div>
  <div class="plan featured">
    <h3 class="plan-title">
      Professional
    </h3>
    <div class="plan-cost"><span class="plan-price">$89</span><span class="plan-type">/ one-time</span></div>
    <ul class="plan-features" style="text-align: left;">
      <li><i class="ion-checkmark"> </i>10 WordPress sites</li>
      <li><i class="ion-checkmark"> </i>6 months of support</li>
      <li><i class="ion-checkmark"> </i>6 months of premium updates</li>
      <li><i class="ion-checkmark"> </i>24/7 Tech Support</li>
    </ul>
    <div class="plan-select"><a href="" data-fsc-action="Add,Checkout" data-fsc-item-path-value="facebook-feed-professional" onclick="this.parentNode.submit(); return false;">BUY NOW</a></div>
  </div>
  <div class="plan">
    <h3 class="plan-title">
      Ultra
    </h3>
    <div class="plan-cost"><span class="plan-price">$169</span><span class="plan-type">/ one-time</span></div>
    <ul class="plan-features" style="text-align: left;">
      <li><i class="ion-checkmark"> </i>20 WordPress sites</li>
      <li><i class="ion-checkmark"> </i>1 year of support</li>
      <li><i class="ion-checkmark"> </i>1 year of premium updates</li>
      <li><i class="ion-checkmark"> </i>24/7 Tech Support</li>
    </ul>
    <div class="plan-select"><a href="" data-fsc-action="Add,Checkout" data-fsc-item-path-value="facebook-feed-ultra" onclick="this.parentNode.submit(); return false;">BUY NOW</a></div>
  </div>
</div>
</div>

            </div>

        </div>
        <!-- /.container -->
        <div class="container">
        <div class="row">
 <div class="col-lg-12 text-center">
               <p id="guarantee" style="    border-top: 4px double #dadada !important;
    border-bottom: 4px double #dadada !important;
    padding: 5px;
    width: 99%;
    margin: 0 auto;
    margin-top: 12px;
    font-weight: bold;
    color: #7e7e7e;
        margin-bottom: -78px;
    font-size: 20px;">We offer a <span>14 Day Money Back Guarantee</span>, so buying is risk-free!</p>
     <p style="
    
    padding: 5px;
    width: 51%;
    margin: 0 auto;
        margin-top: 86px;
    font-weight: bold;
    color: #7e7e7e;
        margin-bottom: -78px;
    font-size: 12px;"><a target="_blank" href="https://www.arrowplugins.com/refund-policy" >(Refund Policy)</a></p>
                </div>
    </div>
    </div>
    </div>
<div style="text-align: center;padding: 30px;">
    <img width="190px" src="<?php echo get_template_directory_uri(); ?>/images/fs.png">
    <a rel="nofollow" target="_blank" href="https://safeweb.norton.com/report/show?url=www.arrowplugins.com">
    <img width="100px" alt="Norton" src="<?php echo get_template_directory_uri(); ?>/images/ns.png"></a>
    <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=rvymT90XUwbL1RJxXjYSjGMbSQT0OyDHkqQ0aheOrTj1ky8msfXZ8JIyLTAU"></script></span>
  </div>
 <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
 
    <!-- /.banner -->

    <!-- Footer -->
<footer style="line-height: 42px;
    height: 40px;
    background: #f7f7f7;
    margin: 0;
    padding: 0;
    font-size: 16px;
        font-weight: bold;">
        <div class="container">
            <div class="row text-center">
                    <p class="copyright text-muted small" style="margin: 0;padding: 0;">Copyright © 2016 - 2017.</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <style type="text/css">
        footer{
            padding: 0 0 12px 0;
        }
    </style>
<!-- jQuery -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
<!-- Owl Carousel -->
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<!-- Main JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!-- Hammer JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/hammer.js"></script>
<!-- ImageLoaded JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.js"></script>
<!-- Sequence JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sequence.js"></script>

<?php wp_footer(); ?>

</body>

</html>@
