<?php /* Template Name: Support */ ?>

<!DOCTYPE html>
<head>
 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Clickit Plugins | Premium WordPress Plugins</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simple, Responsive & Highly Customizable plugins to add an extra functionality into your site." />
	<meta name="keywords" content="wordpress, wordpress plugins, wordpress premium plugins, plugins, social share, social, social flating, social button, popup, popup plugin, wordpress popup plugin, popup builder, wp popup buildier" />
	<meta name="author" content="clickitplugins" />
	<meta name="msvalidate.01" content="5C9D99C96DDE5C5A9895D4A1E481D1B4" />
	<meta name="google-site-verification" content="piICf3uM39zgLVKK9Ynr69U-R2WEYQIxAmqoqaHkUik" />
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png">


	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/simple-line-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
	<!-- Style -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200' rel='stylesheet' type='text/css'>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

	
	<!-- Modernizr JS -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->
<!-- Begin Inspectlet Embed Code -->

<style type="text/css">
	img#wpstats{
		display: none !important;
	}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92795754-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7423KS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header role="banner" id="fh5co-header">
		<div class="fluid-container">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
					<a class="navbar-brand" href="https://www.clickitplugins.com"><span>Clickit Plugins</span></a> 
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a id="home_link" href="https://www.clickitplugins.com"><span>Home</span></a></li>
							<li id="support_li" class="active"><a id="support_link" href="https://www.clickitplugins.com/support" data-nav-section="support"><span>Support</span></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>

		<section id="fh5co-home" data-section="home" style="height:230px;">
			<div class="gradient"></div>
			<div class="container">
				<div class="text-wrap" style="    height: 300px;">
					<div class="text-inner">
						<div class="row" style="width:100%;margin-top: 113px;">
							<div class="col-md-8 col-md-offset-2" style="width: 100%;">
								<h1 class="to-animate">SUPPORT</h1>
								<h2 class="to-animate" style="margin: 0 auto; width: 70%;"><h2 class="to-animate">Always make sure you are running the latest version of WordPress and also the latest version of our themes or plugins on your site. Feel free to ask!</h2></h2>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="fh5co-home" data-section="home" style="height:auto;">
			<div class="gradient"></div>
			<div class="container">
				<div class="text-wrap">
					<div class="text-inner" style="vertical-align: top;">
						<div class="row" style="width:100%">
							<div class="form-style-2 to-animate">
							<h2 id="success_message"></h2>
								<form action="<?php echo get_template_directory_uri(); ?>/sendmail.php" method="POST">
	<label for="field1"><span>Name <span class="required">*</span></span><input  type="text" class="input-field" name="name" value="" required/></label>
	<label for="field2"><span>Email <span class="required">*</span></span><input type="email" class="input-field" name="email" value="" required/></label>
	<label for="field2"><span>Your Site URL <span class="required">*</span></span><input type="text" class="input-field" name="url" value="" required/></label>
	<label for="field4"><span>Regarding <span class="required">*</span></span><select id="plugin_names" name="plugin" class="select-field" required>
	<option value="">Select Plugin</option>
	<option value="Facebook Feed">Facebook Feed</option>
	<option value="Ultra Image Slider">Ultra Image Slider</option>
	</select></label>
		<label for="field45"><span>Version <span class="required">*</span></span>
<input id="free-version" type="radio" name="version" value="Free Version" required style="     width: 20px;
    height: 17px; cursor: pointer;  margin-top: 15px;
    cursor: pointer;"> <label style="display: inline-block;cursor: pointer;" for="free-version">Free </label>
 <input id="premium-version" type="radio" name="version" value="Premium Version" required style="      width: 20px;
    height: 17px;    margin-left: 50px;  
    cursor: pointer;"> <label style="display: inline-block;cursor: pointer;"  for="premium-version">Premium </label>
		</label>
	<label for="field5"><span>Message <span class="required">*</span></span><textarea name="message" class="textarea-field" required="required"></textarea></label>

	<label><span>&nbsp;</span><input type="submit" value="Send Message" /></label>
	</form>
								</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<footer style="line-height: 42px;
    height: 40px;
    background: #f7f7f7;
    margin: 0;
    padding: 0;
    font-size: 16px;
        font-weight: bold;">
        <div class="container">
            <div class="row text-center">
                    <p class="copyright text-muted small" style="margin: 0;padding: 0;">Copyright © 2016 - 2017 clickitplugins.</p>
                </div>
            </div>
        </div>
    </footer>
<!-- jQuery -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
<!-- Owl Carousel -->
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<!-- Main JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!-- Hammer JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/hammer.js"></script>
<!-- ImageLoaded JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.js"></script>
<!-- Sequence JS (Do not remove) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sequence.js"></script>

<script type="text/javascript">
		$('#home_link').click(function(){
   window.location.href='https://www.clickitplugins.com/';
});

        $("#plugin_names").html($('#plugin_names option').sort(function(x, y) {
            return $(x).val() < $(y).val() ? -1 : 1;
        }))
        $("#plugin_names").get(0).selectedIndex = 0;
	</script>
	<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
<?php wp_footer(); ?>
	
</body>
</html>
