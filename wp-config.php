<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cplugins_local');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R5frwLn9Rz0fI3yRk5AuAGWauu4ViD2err9BEevZieccGybF6GsARWDYsC8AtCtA');
define('SECURE_AUTH_KEY',  'csc4y8ai4DQdChXu6oGNhzsNFEE0r7LI26LpeBOMWUzqatgFtlClMEAsBgeailbo');
define('LOGGED_IN_KEY',    'ZfziT06WrdeVK8XB2D7QnwmoLNGvIlWaMTzxIb27DWZ7JefTqRCff4VjJr2DlL3s');
define('NONCE_KEY',        'HPHY3K5P69CAwvy4zMxCFZe3Ha3R0kr3lDFXq4jrfV9r6PMIDAwdZOeSErCoPXGN');
define('AUTH_SALT',        'cyaByxvzzEbm8D1v1orEjGJyEfVBdKJIfhKwbFRJI9UAPuNoTKGsqP7cZRB9HBPW');
define('SECURE_AUTH_SALT', '8jZ8CB4WszjS7laZ8EYJZwaEPz4i9cWfY4i1OfyIgGtbuunCWtEOr75lsmJstDyh');
define('LOGGED_IN_SALT',   'gBmPzxHnpifwj1HCFLO6jyTG1M6VfCJWAl4pA8vWmqh17aHJ1JICXbyyMVBca3nS');
define('NONCE_SALT',       'UabQJfK40s5LydvOguAMCsLjKbfwOVWOlxzA1ki70bbfSyiU8Rk858i5urA8cqZo');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
